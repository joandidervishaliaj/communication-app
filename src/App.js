import React from 'react';
import './App.css';
import Navbar from './components/Navbar';
import Routing from './components/Routing';
import { BrowserRouter as Router } from 'react-router-dom';

/**
 * App is a functional component used as the structure of the app and where other components are rendered.
 * 
 * @return {ReactComponent} Returns basic structure of the app
 */
function App() {
  return (
    <Router>
      <div className="App">
        <Navbar />
        <Routing />
      </div>
    </Router>
  );
}

export default App;