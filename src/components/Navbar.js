import React from 'react';
import { Link } from 'react-router-dom';

/**
 * Navbar is a functional component that displays navigation bar positioned on top of the website.
 * 
 * @return {ReactComponent} Returns the navigation bar.
 */
function Navbar() {

    return (
        <div>
            <nav className="navbar navbar-expand-sm bg-primary navbar-dark">
                <span className="navbar-brand text-white">Communication App</span>
                <ul className="navbar-nav">
                    <Link to='/home'>
                        <li className="nav-item">
                            <span className="nav-link">Home</span>
                        </li>
                    </Link>
                    <Link to='/locations'>
                        <li className="nav-item">
                            <span className="nav-link">Locations</span>
                        </li>
                    </Link>
                    <Link to='/map'>
                        <li className="nav-item">
                            <span className="nav-link">Map</span>
                        </li>
                    </Link>
                </ul>
            </nav>
        </div>
    );
    
}

export default Navbar;