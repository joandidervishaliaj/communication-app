import React from 'react';
import Home from './Home';
import Locations from './Locations';
import Map from './Map';
import { Switch, Route } from 'react-router-dom';
import { Redirect } from 'react-router';

/**
 * Routing is a functional component used for all routing paths of the application.
 * 
 * @return {ReactComponent} Returns the routing paths
 * 
 * @see https://reactjs.org/community/routing.html
 */
function Routing() {

    return (
        <Switch>
            <Route path="/home" exact component={Home} />
            <Route path="/locations" exact component={Locations} />
            <Route path="/map" exact component={Map} />
            <Redirect from="*" to="/home" />
        </Switch>
    );

}

export default Routing;
