import React, { Component } from 'react';
import LocationImage from './LocationImage';

/**
 * Map class contains component for /map path.
 */
export class Map extends Component {

    /**
     * Constructor of the class. Sets the state object and creates a new 
     * BroadcastChannel to listen for messages from Locations
     * 
     * @param {Object} props Object that contains parameters passed to this class
     * 
     * @see https://developer.mozilla.org/en-US/docs/Web/API/Broadcast_Channel_API
     */
    constructor(props) {
        super(props);
    
        this.state = {
            locationAvailable: false,
            location: ''
        };

        const channel = new BroadcastChannel('messenger');

        channel.onmessage = function(e) {
            console.log(e.data);
            
            this.setState({
                locationAvailable: true,
                location: e.data
            });
        }.bind(this);
    }

    /**
     * Renders the Map component in application. 
     * Displays a card where the LocationImage will be displayed when channel receives a message.
     * 
     * @return {ReactComponent} Returns the Map components view
     */
    render() {
        return (
            <div style={{margin: 'auto', width: '90%'}}>
                <br /><br />
                <div className="card">
                    <div className="card-header">
                        <h5 className="card-title">Location Image</h5>
                    </div>
                    <div className="card-body">
                        {
                            this.state.locationAvailable ? 
                            <LocationImage srcName={this.state.location} />
                            :
                            ''
                        }
                    </div>
                </div>
            </div>
        );
    }
    
}

export default Map;
