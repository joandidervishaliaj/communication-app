import React from 'react';
import Location1 from '../location-images/Location_1.png';
import Location2 from '../location-images/Location_2.png';

/**
 * LocationImage is a functional component that returns the 
 * location requested with an img element.
 * 
 * @param {Object} props Object that contains parameters passed to this function
 * @return {ReactComponent} Returns img tag to be rendered in app
 */
function LocationImage(props) {

    if(props.srcName == 'Location_1') {
        return <img src={Location1} style={{width: '100%'}} />;
    } else if(props.srcName == 'Location_2') {
        return <img src={Location2} style={{width: '100%'}} />;
    }
    
}

export default LocationImage;