import React, { Component } from 'react';

/**
 * Locations class contains component for /locations path.
 */
export class Locations extends Component {

    /**
     * This function gets the location when Show button is 
     * clicked and sends it to other BroadcastChannels listening 
     * on the same channel name (used to send location to Map)
     * 
     * @param {string} location Location of the place in Map
     * 
     * @see https://developer.mozilla.org/en-US/docs/Web/API/Broadcast_Channel_API
     */
    showLocation(location) {
        const channel = new BroadcastChannel('messenger');
        channel.postMessage(location);
        channel.close();
    }

    /**
     * Renders the Locations component in application. 
     * Displays a list of locations with Show buttons for each.
     * 
     * @return {ReactComponent} Returns the Locations components view
     */
    render() {
        return (
            <div style={{margin: 'auto', width: '90%'}}>
                <br /><br />
                <div className="card">
                    <div className="card-header">
                        <h5 className="card-title">Choose one of the locations</h5>
                    </div>
                    <div className="card-body">
                        <ul className="list-group">
                            <li className="list-group-item text-right"><span style={{float: 'left'}}><strong>Location 1</strong></span><button className='btn btn-info' onClick={() => this.showLocation('Location_1')}>Show</button></li>
                            <li className="list-group-item text-right"><span style={{float: 'left'}}><strong>Location 2</strong></span><button className='btn btn-info' onClick={() => this.showLocation('Location_2')}>Show</button></li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
    
}

export default Locations;
