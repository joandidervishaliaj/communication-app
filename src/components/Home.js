import React, { Component } from 'react';

/**
 * Home class contains component for /home path.
 */
export class Home extends Component {

    /**
     * Renders the Home component in application. Displays a simple 
     * welcome page with some basic information about the creator.
     * 
     * @return {ReactComponent} Returns the Home components view
     */
    render() {
        return (
            <div className="card">
                <div className="card-header">
                    Communication App
                </div>
                <div className="card-body">
                    <div className="container-fluid">
                        <div className="text-center">
                            <br />
                            <img src={'https://raw.githubusercontent.com/wingify/across-tabs/master/across-tabs.png'}
                                className="img-responsive center-block"
                                style={{height: '300px'}} />
                        </div>
                        <br /><br />
                        <div className="text-center">Developed by:</div>
                        <div className="text-center">
                            <h3>Joandi Dervishaliaj</h3>
                        </div>
                        <div className="text-center">@joandi_dervishaliaj</div>
                        <div className="text-center">
                            <a href="http://www.joandidervishaliaj.com">www.joandidervishaliaj.com</a>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default Home;
