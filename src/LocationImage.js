import React from 'react';

const LocationImage = props => {
    console.log(props);
    return (
        <img src={require(process.env.PUBLIC_URL + props.src)} style={{width: '100%'}} />
    )
} 

export default LocationImage;